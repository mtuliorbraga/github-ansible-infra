#!/bin/bash

# Check all YAML files syntax
# $0 [--permissive]

find -iname '*.yml' \
	! -name 'circle.yml' \
	! -path './roles_global/*' \
	| xargs -I% ansible-lint %

RC=$?

if [ $# -ne 0 ] && [ "$1" == "--permissive" ]
then
	if [ $RC -ne 0 ];
  then
		echo "################################################################"
		echo "#>>> Skipping some errors found on YAML files by script arugment:"
		echo "$ $0 $@"
	fi

	exit 0
else
	echo "################################################################"
	echo "#>>>>>  ATTENTION: You should consider review your code syntax."
	echo "#>>>>>             Check errors above and fix it."
	echo "################################################################"
	exit $RC
fi
