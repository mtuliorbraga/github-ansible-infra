#!/bin/bash

# Script based on CircleCI DOC https://circleci.com/docs/parallel-manual-setup/#manual-balancing
# Run parallel scripts with prefix test/run-test-*.sh

#test_script=$(find -name 'run-test-*.sh' | sort | awk "NR % ${CIRCLE_NODE_TOTAL} == ${CIRCLE_NODE_INDEX}")

if [ $CIRCLE_NODE_TOTAL -eq 1 ]
then

  bash ./test/run-test-syntax.sh --permissive
  bash ./test/run-test-roles.sh
  bash ./test/run-test-playbook.sh

else

  case $CIRCLE_NODE_INDEX in
    0) CMD_RUNNER='bash ./test/run-test-syntax.sh --permissive' ;;
    1) CMD_RUNNER='bash ./test/run-test-roles.sh' ;;
    2) CMD_RUNNER='bash ./test/run-test-playbook.sh' ;;
    3) unset CMD_RUNNER ;;
  esac

  if [ -z "$CMD_RUNNER" ]
  then
      echo "[$0] Container/Runner [$CIRCLE_NODE_INDEX] has no jobs to run."
  else
      echo "[$0] Running parallel script in container [$CIRCLE_NODE_INDEX]: [$CMD_RUNNER]"
      $CMD_RUNNER
  fi

fi
