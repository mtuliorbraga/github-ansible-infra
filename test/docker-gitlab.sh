#!/bin/bash

# Project Name on GITLAB for CI
PROJ_NAME=mtuliorbraga/github-ansible-infra

GITLAB_REGISTRY_LOGIN() {
  docker login registry.gitlab.com
}

#DOCKER_PULL() { }

DOCKER_PUSH() {
  docker push registry.gitlab.com/${PROJ_NAME}
}

DOCKER_BUILD_ALL() { 
  DOCKER_FILE_GET;

  for FILE in $DOCKERFILES; do

    NAME=$(echo $FILE |awk -F'dockerfile-' '{print$2}')

    docker build -f $FILE -t registry.gitlab.com/${PROJ_NAME}:${NAME} .

  done
}

#DOCKER_BUILD_ONE() { }

DOCKER_FILE_GET() {
  DOCKERFILES=$(find -iname 'dockerfile-*')
}

DOCKER_FILE_LIST() {
  DOCKER_FILE_GET;
  echo "# List Dockerfiles   [  filename : remote name ]: "
  for FILE in $DOCKERFILES; do echo -ne "$FILE \t : $(echo $FILE |awk -F'dockerfile-' '{print$2}')\n"; done
}

# Remove all images from repo
DOCKER_RMI_ALL() {
  DOCKER_FILE_GET;

  for FILE in $DOCKERFILES; do

    NAME=$(echo $FILE |awk -F'dockerfile-' '{print$2}')
    docker rmi registry.gitlab.com/${PROJ_NAME}:${NAME}

  done

}

DOCKER_IMAGE_LIST() {
  docker images registry.gitlab.com/${PROJ_NAME}
}

case $1 in
  'login') GITLAB_REGISTRY_LOGIN ;;
  'list-files') DOCKER_FILE_LIST ;;
  'list-images') DOCKER_IMAGE_LIST ;;
  'build-all') DOCKER_BUILD_ALL ;;
  'rmi-all' ) DOCKER_RMI_ALL ;;
  *) echo "Option not found" ;;
esac
