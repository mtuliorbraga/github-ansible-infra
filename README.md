Ansible Infrastructure
======================

[![Project Status: Concept - nitial development is in progress, but there has not yet been a stable, usable release suitable for the public.](http://www.repostatus.org/badges/latest/wip.svg)](http://www.repostatus.org/#wip)
[![CircleCI](https://circleci.com/gh/mtulio/ansible-infra.svg?style=svg)](https://circleci.com/gh/mtulio/ansible-infra) 
[![build status](https://gitlab.com/mtuliorbraga/github-ansible-infra/badges/master/build.svg)](https://gitlab.com/mtuliorbraga/github-ansible-infra/commits/master)


Overview
========

Main repository to keep configuration management using Ansible.
Ansible version: 2.2


Getting Started
===============

`git clone git@github.com:mtulio/ansible-infra.git --recursive`


ROLES
=====

### LetsEncrypt

* Shell usage:

`certbot-auto certonly -a webroot --webroot-path=/var/www/html/ -d ict-eng.net  -d www.ict-eng.net -d app.ict-eng.net
sudo openssl dhparam -out /etc/ssl/certs/dhparam.pem 2048`

TODO: 

* --csr

Oficial doc: http://letsencrypt.readthedocs.io/en/latest/using.html

---
---

## Useful commands

### Ansible

#### Config

#### Inventory

#### Modules

##### Ping

##### Setup

### GIT

#### Clone recursively 

#### Submodule - ADD

#### Submodule - PULL

## DOCS and REFS

* [Ansible Best Practices](http://docs.ansible.com/ansible/playbooks_best_practices.html)
* [Ansible Playbook Roles and Include Statements](http://docs.ansible.com/ansible/playbooks_roles.html)
