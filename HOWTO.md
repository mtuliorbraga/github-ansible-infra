# ANSIBLE HowTo

Some tips to use ansible, ad-hoc commands, install procedures, etc

## AD-HOC

###  MISC

* Run `ls` command, [b]ecoming root, with custom inventory file in `databases` hostgroup:

`ansible -b -i myinventory databases -a "ls -la"`

* Create a directory, [b]ecoming root, with custom inventory file in `databases` hostgroup:

`ansible -b -i myinventory databases -a "path=/opt/logs state=directory"`


* Run ad-hoc in N forks. Ex. 1000 forks:

`ansible all -m ping -f 1000`

## Managing Parallelism 

* Ad-hoc - running of 100 parallel

 `ansible all -m ping -f 100`

* Playbook - running `serial` of 3 servers

> It will run whole playbook with only 3 servers, and repeat it

```YAML
- hosts: ec2
  become: yes
  serial: 3
  tasks:
    - name: Ping server
      ping:
```

* Playbook - running `serial` of 30% os servers

```YAML
- hosts: ec2
  become: yes
  serial: "30%"
[...]
```

* Playbook - running `serial` in batches of hosts 

```YAML
- hosts: ec2
  become: yes
  serial:
    - 1
    - 5
    - "20%"
[...]
```

* Playbook - running `serial` with fail tolerance

> Example below will accept only 3 hosts fails

```YAML
- hosts: ec2
  become: yes
  max_fail_percentage: 30
  serial: 10
[...]
```

## INVENTORY

* List all hosts from the inventory

`ansible all -m setup --list-hosts `

* List hosts for an given group

`ansible <GROUP_NAME> --list-hosts`

* Limit by hostname/IP

`ansible all --list-hosts -l 10.10.100.77,`

### Inventory file

* Inventory with custom hostgroups

```ansible
[local]
localhost ansible_connection=local

[webserver-us1]
webserver-www ansible_host=server1.us.mydomain.com ansible_port=2290
webserver-static ansible_host=server2.us.mydomain.com ansible_port=2290

[webserver-us1:vars]
log_folders=/opt/logs

[webserver-br1]
webserver-www ansible_host=server1.br.mydomain.com ansible_port=2290

[all-webservers]
webserver-us1
webserver-br1

```

### Custom inventory

Just write a script to return the list of hosts in JSON format, and call it:

* Playbook:

```shell
ansible-playbook playbook.yml -i ./dynamic.py
```

* Ad-hoc

```shell
ansible all -i ./dynamic.py -m ping
```

### Dynamic Inventory: AWS

Considering that ec2.py is in environment/production/ directory

* List all hosts calling it directly

`./environment/production/ec2.py --list`

* List EC2 host information

`./environment/production/ec2.py --host 10.10.10.10`

* Refresh EC2 inventory

`ansible -i environments/production/ec2.py tag_Name_SERVER_NAME_01 -m meta -a "refresh_inventory"`

* Lookup server by Tag Name

`ansible -i environments/production/ec2.py tag_Name_SERVER_NAME_01 --list-hosts`

### Hybrid inventory

To use both static and dynamic inventory you should put all the script to manage inventory inside a dir, and set it dir in `ansible.cfg`:

* ansible.cfg

```
[defaults]
inventory = ./inventory/prod/
```

* Directory `./inventory/prod/`


* Static inventory `./inventory/prod/inventory`

```
[tag_ansible_host_web_cluster]

[web-cluster:children]
tag_ansible_host_web_cluster

[local]
localhost ansible_connection=local ansible_host=localhost

```


## GALAXY

* Install roles from a file [requirements.yml](http://docs.ansible.com/ansible/galaxy.html#installing-multiple-roles-from-a-file)

## ANSIBLE TOWER

* Installing

1. Download latest version from https://www.ansible.com/tower-editions
1. extract file `ansible-tower-setup-latest.tar.gz`
1. less README.md
1. `vim inventory` and set these vars: `admin_password`. `pg_password` and `rabbitmq_password`
1. `./setup.sh`
1. Open the Web UI: https://your_ip
1. Create a Request license and upload it to complete the registration


___

# Ansible FAQ

## Static and Dynamic inventories

1) The correct option to use your own static inventory with ansible ad-hoc command is ansible local -L localfile -m ping
  Correct answer: False

  Explanation
  The correct option is -i so the command would be ansible local -i localfile -m ping

2) A dynamic inventory file that is executable is expected to respond to the --list as an option.
  Correct answer: True

  Explanation
  The dynamic inventory program should respond to the --list and --host [hostname] options when called. More information about developing your own dynamic inventories can be found here. http://docs.ansible.com/ansible/dev_guide/developing_inventory.html

3) The default static inventory for ansible is located at /etc/ansible/default
  Correct answer: False

  Explanation
  The correct location is /etc/ansible/hosts

4) If you use the -i option to select a static hosts file and its marked as executable, its still treated as a static inventory.
  Correct answer: False

  Explanation
  If a file used with the -i option is executable its expected to be a dynamic inventory.

5) A Dynamic inventory program has its output as plain text.
  Correct answer: False

  Explanation
  The output should be in json format.

## PARALLELISM

1) If you use a larger no of forks than you have servers to send tasks to, ansible will stop.
  Correct answer: False

  Explanation
  Ansible will ignore the extras.

2) You can change the no of forks ansible uses from the /etc/ansible/ansible.cfg file.
  Correct answer: True

3) You can adjust the parallelism inside a playbook.
  Correct answer: True

  Explanation
  The option is called serial:

4) The default no of forks that ansible uses it 8.
  Correct answer: False

  Explanation
  The default no is 5.

5) The maximum no of forks is 100
  Correct answer: False

  Explanation
  The limit relates to memory but is larger than 100.

