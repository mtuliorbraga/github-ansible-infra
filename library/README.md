# Ansible Modules

Custom modules, put them here.

## Testing modules

* Clone ansible repository to use hacking tools

```shell
git clone git://github.com/ansible/ansible.git --recursive /tmp/test-ansible
source /tmp/test-ansible/hacking/env-setup
```

* Test the module without argument

`/tmp/test-ansible/hacking/test-module -m ./hello_world.py`

* Test the module with arguments

`/tmp/test-ansible/hacking/test-module -m ./hello_world.py -a "command=hostname"`

## References

* [Modules Developing Documentation](http://docs.ansible.com/ansible/dev_guide/developing_modules.html)
