#!/usr/bin/python
#coding: utf-8 -*-

# (c) 2016-2017, Marco Tulio R Braga <braga@mtulio.eng.br>
#
# This module is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this software.  If not, see <http://www.gnu.org/licenses/>.

DOCUMENTATION = '''
---
module: hello_world
version_added: 0.1
author: "Marco Tulio R Braga (@mtulio)"
short_description: Ansible Module Hello World
description:
   - Print Hello world and some functions in commands argument
options:
   command:
     description:
        - Command to be executed using the module
     required: false
     choices: ['date', 'hostname']
     default: date
     version_added: "2.1"

requirements: ["datetime"]
'''

import datetime
import socket

def _command_date(module):
    mod_result = str(datetime.datetime.now())
    module.exit_json(changed = False, result = mod_result)

def _command_hostname(module):
    mod_result = str(socket.gethostname())
    module.exit_json(changed = False, result = mod_result)

def main():
    module = AnsibleModule(
        argument_spec = dict(
            command = dict(required=False, default='date', choices=['date', 'hostname'])
        ),
    )

    if module.params['command'] == 'date':
        _command_date(module)

    if module.params['command'] == 'hostname':
        _command_hostname(module)

# import module snippets
from ansible.module_utils.basic import *
if __name__ == '__main__':
    main()
